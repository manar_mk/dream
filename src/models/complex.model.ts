import { Exercise } from './exercise.model';
export class Complex {
  id: number;
  title: string;
  description: string;
  image: string;
  exercises: Exercise[];
  currentExerciseIndex = 0;
  videoLink?: string;

  constructor(complex: any) {
    this.id = complex.id;
    this.title = complex.title;
    this.description = complex.description;
    this.image = complex.image;
    this.videoLink = complex['video_link'];
    this.exercises = complex.exercises.map(item => {
      return new Exercise(item);
    });
  }

  getFirstExercise() {
    this.currentExerciseIndex = 0;
    return this.exercises[0];
  }

  prevExercise() {
    this.currentExerciseIndex--;
    return this.getCurrentExercise();
  }

  nextExercise() {
    this.currentExerciseIndex++;
    return this.getCurrentExercise();
  }

  setCurrentExercise(exercise: Exercise) {
    this.currentExerciseIndex = this.getExerciseIndex(exercise);
    return this.getCurrentExercise();
  }

  getExerciseIndex(exercise: Exercise) {
    return this.exercises.indexOf(exercise);
  }

  getCurrentExercise() {
    return this.exercises[this.currentExerciseIndex] || this.getFirstExercise();
  }

  isLastExercise(){
    return this.currentExerciseIndex === this.exercises.length - 1;
  }
  isFirstExercise(){
    return this.currentExerciseIndex === 0;
  }
}
