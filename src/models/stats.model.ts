export class StatsItem {
  title: string;
  dates: Date[];

  constructor(stats: any) {
    this.title = stats.title;
    this.dates = stats.dates;
  }
}
