export class Exercise {
  id: number;
  title: string;
  image: string;
  description: string;
  duration: number;

  constructor(exercise: any) {
    this.id = exercise.id;
    this.title = exercise.title;
    this.image = exercise.image;
    this.description = exercise.description;
    this.duration = exercise.duration;
  }
}
