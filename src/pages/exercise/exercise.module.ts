import { PipesModule } from './../../pipes/pipes.module';
import { ExerciseModalPageModule } from './../exercise-modal/exercise-modal.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExercisePage } from './exercise';

@NgModule({
  declarations: [
    ExercisePage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(ExercisePage),
    ExerciseModalPageModule
  ],
})
export class ExercisePageModule {}
