import { AudioProvider } from './../../providers/audio/audio';
import { Component } from '@angular/core';

import { SocialSharing } from '@ionic-native/social-sharing';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  Platform,
  LoadingController
} from 'ionic-angular';

import { ComplexPage } from './../complex/complex';
import { CongratsPage } from './../congrats/congrats';

import { StatsProvider } from './../../providers/stats/stats';

import { Exercise } from './../../models/exercise.model';
import { Complex } from '../../models/complex.model';
import { Insomnia } from '@ionic-native/insomnia';

/**
 * Generated class for the ExercisePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-exercise',
  templateUrl: 'exercise.html'
})
export class ExercisePage {
  complex: Complex;
  exercise: Exercise;

  isRunning: boolean;
  timer: any = null;
  timerValue: number;

  isPrepared: boolean;
  prepareTimer: any = null;
  prepareValue: number;

  isSleeping: boolean = false;

  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private socialSharing: SocialSharing,
    private statsProvider: StatsProvider,
    private audioProvider: AudioProvider,
    private platform: Platform,
    private insomnia: Insomnia
  ) {
    this.platform.pause.subscribe(() => {
      this.stop();
      this.isSleeping = true;
    });

    this.platform.resume.subscribe(() => {
      this.isSleeping = false;
    });
  }

  ionViewDidLoad() {
    this.startLoading();

    const voices$ = this.audioProvider.loadVoices();

    voices$.subscribe(result => {
      this.stopLoading();
      this.isSleeping = false;
      this.complex = this.navParams.data.complex;
      const exercise = this.navParams.data.exercise;
      this.initExercise(exercise);
    });
  }

  ionViewDidEnter(){
    this.stop();
    this.isSleeping = false;
  }

  ionViewWillLeave() {
    this.stop();
    this.isSleeping = true;
  }

  back() {
    this.navCtrl.setRoot(ComplexPage, {
      complex: this.complex
    });
  }

  share() {
    this.socialSharing.share(
      `Выполняю упражнение ${
        this.exercise.title
      } в приложении "Мечтай и стройней"`
    );
  }

  stop() {
    this.isRunning = false;
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
    this.insomnia
      .allowSleepAgain()
      .then(
        () => console.log('allowed sleep again'),
        () => console.log('cant go to sleep')
      );
  }

  start() {
    if (this.isSleeping === false) {
      this.audioProvider.playExerciseStart();
      this.isRunning = true;
      setTimeout(() => {
        if (this.timer === null) {
          this.insomnia.keepAwake().then(
            () => {
              this.timer = setInterval(() => {
                if (this.isSleeping === false) {
                  if (this.timerValue > 0) {
                    this.timerValue--;
                  }
                  if (this.timerValue === 0) {
                    this.audioProvider.playExerciseEnd();
                    clearInterval(this.timer);
                    setTimeout(() => {
                      this.next();
                    }, 3000);
                  }
                } else {
                  this.stop();
                }
              }, 1000);
            },
            () => {
              this.stop();
            }
          );
        }
      }, 2000);
    }
  }

  prev() {
    if (!this.isPrevDisabled()) {
      const exercise = this.complex.prevExercise();
      this.initExercise(exercise);
      return;
    }
  }

  next() {
    if (!this.isNextDisabled()) {
      const exercise = this.complex.nextExercise();
      this.initExercise(exercise);
      return;
    }
    this.complete();
  }

  complete() {
    this.stop();
    this.audioProvider.playComplexEnd();
    this.statsProvider.updateStats(this.complex).subscribe(data => {
      this.navCtrl.setRoot(CongratsPage, {
        complex: this.complex
      });
      console.log(data);
    }, error=>{
      console.log(error);
    });
  }

  isNextDisabled(): boolean {
    return this.complex && this.complex.isLastExercise();
  }

  isPrevDisabled(): boolean {
    return this.complex && this.complex.isFirstExercise();
  }

  private initExercise(exercise: Exercise) {
    this.stop();
    this.complex.setCurrentExercise(exercise);

    this.exercise = this.complex.getCurrentExercise();
    this.timerValue = this.exercise.duration;
    this.initPrepare();
  }

  private initPrepare() {
    this.isPrepared = false;
    this.prepareValue = 5;

    if (this.isSleeping === false) {
      if (this.isPrevDisabled()) {
        this.audioProvider.playComplexStart();
      } else {
        this.audioProvider.playExerciseNext();
      }

      // setTimeout(() => {
      if (this.prepareTimer === null) {
        this.prepareTimer = setInterval(() => {
          this.prepareValue--;
          if (this.prepareValue === 0) {
            this.isPrepared = true;
            clearInterval(this.prepareTimer);
            this.prepareTimer = null;

            this.start();
          }
        }, 1000);
      }
    }
    // }, 0);
  }

  private startLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'Загрузка...'
      });
    }
    this.loading.present();
  }

  private stopLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
