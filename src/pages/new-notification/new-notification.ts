import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from 'ionic-angular';


/**
 * Generated class for the NewNotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-notification',
  templateUrl: 'new-notification.html'
})
export class NewNotificationPage {
  newDate = '09:00';

  days = [
    {
      index: 1,
      title: 'Пн',
      checked: false
    },
    {
      index: 2,
      title: 'Вт',
      checked: false
    },
    {
      index: 3,
      title: 'Ср',
      checked: false
    },
    {
      index: 4,
      title: 'Чт',
      checked: false
    },
    {
      index: 5,
      title: 'Пт',
      checked: false
    },
    {
      index: 6,
      title: 'Сб',
      checked: false
    },
    {
      index: 7,
      title: 'Вс',
      checked: false
    }
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewNotificationPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  save() {
    this.viewCtrl.dismiss({
      time: this.newDate,
      days: this.days
        .filter(item => {
          return item.checked === true;
        })
        .map(item => {
          return item.index;
        })
    });
  }
}
