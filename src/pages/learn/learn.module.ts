import { PipesModule } from './../../pipes/pipes.module';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LearnPage } from './learn';

@NgModule({
  declarations: [
    LearnPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(LearnPage),
    ComponentsModule
  ],
})
export class LearnPageModule {}
