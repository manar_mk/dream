import { ComplexesPage } from './../complexes/complexes';
import { ComplexPage } from './../complex/complex';
import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Complex } from './../../models/complex.model';

/**
 * Generated class for the LearnPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-learn',
  templateUrl: 'learn.html'
})
export class LearnPage {
  complex: Complex;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sanitizer: DomSanitizer
  ) {}

  ionViewDidLoad() {
    this.complex = this.navParams.data.complex;
  }

  back() {
    this.navCtrl.setRoot(ComplexesPage);
  }

  startComplex() {
    this.navCtrl.setRoot(ComplexPage, {
      complex: this.complex
    });
  }

  getVideoLink() {
    if (this.complex && this.complex.videoLink) {
      var video_id = this.complex.videoLink.split('v=')[1];
      var ampersandPosition = video_id.indexOf('&');
      if (ampersandPosition != -1) {
        video_id = video_id.substring(0, ampersandPosition);
      }

      return this.sanitizer.bypassSecurityTrustResourceUrl(
        `https://www.youtube.com/embed/${video_id}?enablejsapi=1&fs=1&playsinline=1`
      );
    }
    return '';
  }
}
