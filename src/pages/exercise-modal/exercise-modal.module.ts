import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExerciseModalPage } from './exercise-modal';

@NgModule({
  declarations: [
    ExerciseModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ExerciseModalPage),
  ],
})
export class ExerciseModalPageModule {}
