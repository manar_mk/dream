import { ComplexProvider } from './../../providers/complex/complex';
import { Exercise } from './../../models/exercise.model';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from 'ionic-angular';

/**
 * Generated class for the ExerciseModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-exercise-modal',
  templateUrl: 'exercise-modal.html'
})
export class ExerciseModalPage {
  exercise: Exercise;
  duration: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private complexProvider: ComplexProvider
  ) {}

  ionViewDidLoad() {
    this.exercise = this.navParams.data.exercise;
    this.duration = this.exercise.duration;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  save(){
    this.complexProvider.updateTimer(this.exercise, this.duration);
    this.dismiss();
  }
}
