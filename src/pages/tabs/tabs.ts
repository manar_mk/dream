import { Component } from '@angular/core';

import { HomePage } from './../home/home';
import { ComplexesPage } from './../complexes/complexes';
import { SettingsPage } from './../settings/settings';
import { StatsPage } from './../stats/stats';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tabHome = HomePage;
  tabComplexes = ComplexesPage;
  tabStats = StatsPage;
  tabSettings = SettingsPage;

  constructor() {

  }

  update() {
  }

  share(){
  }


}
