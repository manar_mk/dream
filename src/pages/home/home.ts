import { FcmProvider } from './../../providers/fcm/fcm';
import { Component, ViewChild } from '@angular/core';
import { NavController, Content } from 'ionic-angular';

import { SocialSharing } from '@ionic-native/social-sharing';
import { tap } from 'rxjs/operators';

import {ToastController} from 'ionic-angular';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Content) content: Content;

  isCollapsed = false;

  constructor(
    public fcm: FcmProvider,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    private socialSharing: SocialSharing,

  ) {
  }

  ionViewDidLoad() {
    this.initInfo();
    this.fcm.getToken();
    this.fcm.listenNotifications().pipe(
      tap(msg=>{
        const toast = this.toastCtrl.create({
          message: msg.body,
          duration: 3000
        });
        toast.present();
      })
    ).subscribe()
  }

  ionViewWillEnter(){
    this.initInfo();
  }

  share() {
    this.socialSharing.share('Я пользуюсь приложением "Мечтай и стройней"');
  }

  openComplexes() {
    this.initInfo();
    this.navCtrl.parent.select(1);
  }

  collapseInfo() {
    this.isCollapsed = !this.isCollapsed;
    if (this.isCollapsed) {
      this.scroll();
    } else {
      this.content.scrollToTop();
    }
  }

  scroll() {
    const info = document.getElementById('info');
    this.content.scrollTo(0, info.offsetTop - 70, 300);
  }

  initInfo(){
    this.isCollapsed = false;
    this.content.scrollToTop();
  }
}
