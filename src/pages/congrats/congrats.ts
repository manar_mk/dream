import { Component } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ComplexesPage } from './../complexes/complexes';
import { ComplexPage } from './../complex/complex';
import { Complex } from './../../models/complex.model';

/**
 * Generated class for the CongratsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-congrats',
  templateUrl: 'congrats.html'
})
export class CongratsPage {
  complex: Complex;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private socialSharing: SocialSharing
  ) {}

  ionViewDidLoad() {
    this.complex = this.navParams.data.complex;
  }

  share() {
    this.socialSharing.share(
      `Я завершил комплекс ${this.complex.title} в приложении "Мечтай и стройней"`
    );
  }

  repeat(){
    this.navCtrl.setRoot(ComplexPage, {
      complex: this.complex
    })
    this.navCtrl.parent.select(1);
  }

  back(){
    this.navCtrl.setRoot(ComplexesPage);
    this.navCtrl.parent.select(1);
  }

  stats(){
    this.navCtrl.setRoot(ComplexesPage);
    this.navCtrl.parent.select(2);
  }
}
