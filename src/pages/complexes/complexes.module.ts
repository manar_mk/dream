import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComplexesPage } from './complexes';

@NgModule({
  declarations: [
    ComplexesPage
  ],
  imports: [
    IonicPageModule.forChild(ComplexesPage),
    ComponentsModule
  ],
})
export class ComplexesPageModule {}
