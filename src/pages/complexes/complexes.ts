import { Complex } from './../../models/complex.model';
import { ComplexProvider } from './../../providers/complex/complex';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  LoadingController
} from 'ionic-angular';

/**
 * Generated class for the ComplexesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-complexes',
  templateUrl: 'complexes.html'
})
export class ComplexesPage {
  complexList: Complex[];
  error: string;
  loading;

  constructor(
    public navCtrl: NavController,
    private complexService: ComplexProvider,
    private socialSharing: SocialSharing,
    public loadingCtrl: LoadingController
  ) {
    this.startLoading();
    this.complexService.getComplexes().subscribe(
      complexList => {
        this.complexList = complexList;
        this.stopLoading();
      },
      error => {
        this.error = error.error;
        this.stopLoading();
      }
    );
  }

  private startLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'Загружаем комплексы...'
      });
    }
    this.loading.present();
  }

  private stopLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  share() {
    this.socialSharing.share('Я пользуюсь приложением "Мечтай и стройней"');
  }
}
