import { NewNotificationPageModule } from './../new-notification/new-notification.module';
import { NotificationsModalPageModule } from './../notifications-modal/notifications-modal.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsPage } from './settings';

@NgModule({
  declarations: [
    SettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsPage),
    NotificationsModalPageModule,
    NewNotificationPageModule
  ],
})
export class SettingsPageModule {}
