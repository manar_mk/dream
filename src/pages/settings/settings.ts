import { AudioProvider } from './../../providers/audio/audio';
import { SettingsProvider } from './../../providers/settings/settings';
import { NotificationsModalPage } from './../notifications-modal/notifications-modal';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  LoadingController
} from 'ionic-angular';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  melody: string;
  voice: string;
  notifications: string;
  isNotificationsEnabled: boolean;

  error;
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    private audioProvider: AudioProvider,
    private settingsProvider: SettingsProvider
  ) {
    this.settingsProvider.soundsEnabledSubject$.subscribe(data=>{
      this.isNotificationsEnabled = data;
    })
  }

  ionViewDidLoad() {
    this.startLoading();
    this.isNotificationsEnabled = this.settingsProvider.isSoundsEnabled();
    const voice$ = this.settingsProvider.getVoice();

    voice$.subscribe(
      voice => {
        this.voice = voice;
        this.stopLoading();
      },
      error => {
        this.error = 'Произошла ошибка на сервере';
        this.stopLoading();
      }
    );
  }

  notifyModal() {
    const modal = this.modalCtrl.create(NotificationsModalPage, {});
    modal.present();
  }

  notificationsEnabledUpdate(value){
    this.isNotificationsEnabled = value;
    this.settingsProvider.setSoundsEnabled(value);
  }

  voiceUpdate(newValue: string) {
    this.startLoading();

    this.audioProvider.stop(this.audioProvider.settingsMaleAudio);
    this.audioProvider.stop(this.audioProvider.settingsFemaleAudio);
    this.audioProvider.stop(this.audioProvider.settingsMelody1Audio);
    this.audioProvider.stop(this.audioProvider.settingsMelody2Audio);
    this.audioProvider.stop(this.audioProvider.settingsMelody3Audio);

    this.settingsProvider.setVoice(newValue).subscribe(data => {
      this.voice = newValue;
      this.stopLoading();
    });
  }

  playAudio(audio) {
    switch (audio) {
      case 'male':
        this.audioProvider.play(this.audioProvider.settingsMaleAudio, true);
        break;
      case 'female':
        this.audioProvider.play(this.audioProvider.settingsFemaleAudio, true);
        break;
      case 'melody1':
        this.audioProvider.play(this.audioProvider.settingsMelody1Audio, true);
        break;
      case 'melody2':
        this.audioProvider.play(this.audioProvider.settingsMelody2Audio, true);
        break;
      case 'melody3':
        this.audioProvider.play(this.audioProvider.settingsMelody3Audio, true);
        break;

      default:
        break;
    }
  }

  private startLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'Загружаем настройки...'
      });
    }
    this.loading.present();
  }

  private stopLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
