import { StatsProvider } from './../../providers/stats/stats';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from 'ionic-angular';
import { StatsItem } from '../../models/stats.model';

/**
 * Generated class for the StatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-stats',
  templateUrl: 'stats.html'
})
export class StatsPage {
  stats: StatsItem[];
  loading;
  error;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public statsProvider: StatsProvider,
    public loadingCtrl: LoadingController
  ) {
    this.startLoading();
    this.statsProvider.getStatsFromServer().subscribe(
      data => {
        this.stats = data;
        this.stopLoading();
      },
      error => {
        this.getStatsFromLocal();
      }
    );
  }

  updateFromLocal(){
    this.startLoading();
    this.getStatsFromLocal();
  }

  getStatsFromLocal(){
    this.statsProvider.getStatsFromLocal().subscribe(
      data => {
        this.stats = data;
        this.stopLoading();
      },
      error => {
        this.error = 'Не удалось загрузить статистику';
        this.stopLoading();
      }
    );
  }

  private startLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'Загружаем статистику...'
      });
    }
    this.loading.present();
  }

  private stopLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
