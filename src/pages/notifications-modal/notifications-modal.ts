import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  ModalController
} from 'ionic-angular';
import { NotificationsProvider } from '../../providers/notifications/notifications';
import { NewNotificationPage } from '../new-notification/new-notification';

/**
 * Generated class for the NotificationsModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifications-modal',
  templateUrl: 'notifications-modal.html'
})
export class NotificationsModalPage {
  loading;
  notifications = [];
  loaded: boolean = false;
  error: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private notificationsProvider: NotificationsProvider,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController
  ) {}

  dismiss() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    this.startLoading();
    this.notificationsProvider.getNotificationsFromServer().subscribe(
      (data: any) => {
        this.notifications = data;
        this.stopLoading();
        this.loaded = true;
      },
      error => {
        this.getNotificationsFromLocal();
      }
    );
  }

  updateFromLocal(){
    this.startLoading();
    this.getNotificationsFromLocal();
  }

  getNotificationsFromLocal() {
    this.error = '';
    this.notificationsProvider.getNotificationsFromLocal().subscribe(
      (data: any) => {
        this.notifications = data;
        this.stopLoading();
        this.loaded = true;
      },
      error => {
        this.stopLoading();
        this.loaded = true;
        this.error = 'Не удалось загрузить напоминания';
      }
    );
  }

  remove(notification) {
    this.startLoading();
    this.notificationsProvider.deleteNotification(notification).subscribe(
      (data: any) => {
        const index = this.notifications.indexOf(notification);
        if (index == 0) {
          this.notifications = [];
        } else {
          this.notifications.splice(index, 1);
        }
        this.notificationsProvider.saveNotifications(this.notifications);
        this.stopLoading();
      },
      error => {
        this.stopLoading();
      }
    );
  }

  private startLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'Загружаем напоминания...'
      });
    }
    this.loading.present();
  }

  private stopLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  getDayTitle(day) {
    switch (day) {
      case 1:
        return 'Пн';
      case 2:
        return 'Вт';
      case 3:
        return 'Ср';
      case 4:
        return 'Чт';
      case 5:
        return 'Пт';
      case 6:
        return 'Сб';
      case 7:
        return 'Вс';
      default:
        break;
    }
  }

  updateNotification(value, notification) {
    notification.enabled = value;
    this.notificationsProvider.updateNotification(notification).subscribe(
      data => {
        this.notificationsProvider.saveNotifications(this.notifications);
      },
      error => {
        notification.enabled = !value;
      }
    );
  }

  addNew() {
    const newModal = this.modalCtrl.create(NewNotificationPage);
    newModal.onDidDismiss(data => {
      if (data) {
        const newNotification = {
          time: data.time,
          days: data.days,
          enabled: true
        };
        this.startLoading();
        this.notificationsProvider
          .createNotification(newNotification)
          .subscribe(
            (data: any) => {
              this.notifications.push(data);
              this.notificationsProvider.saveNotifications(this.notifications);
              this.stopLoading();
            },
            error => {
              this.stopLoading();
            }
          );
      }
    });
    newModal.present();
  }
}
