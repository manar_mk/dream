import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { ComplexPage } from './complex';

import { ComponentsModule } from './../../components/components.module';

@NgModule({
  declarations: [
    ComplexPage
  ],
  imports: [
    IonicPageModule.forChild(ComplexPage),
    ComponentsModule
  ],
})
export class ComplexPageModule {}
