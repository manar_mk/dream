import { Component } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ComplexesPage } from './../complexes/complexes';
import { ExercisePage } from './../exercise/exercise';
import { Complex } from './../../models/complex.model';

/**
 * Generated class for the ComplexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-complex',
  templateUrl: 'complex.html'
})
export class ComplexPage {
  complex: Complex;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private socialSharing: SocialSharing
  ) {}

  ionViewDidLoad() {
    this.complex = this.navParams.data.complex;
  }

  back() {
    this.navCtrl.setRoot(ComplexesPage);
  }

  share() {
    this.socialSharing.share(`Зацените комплекс ${this.complex.title} в приложении "Мечтай и стройней"`);
  }

  startComplex(){
    this.navCtrl.setRoot(ExercisePage, {
      complex: this.complex
    });
  }
}
