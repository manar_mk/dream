import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';

import { StatsItem } from './../../models/stats.model';
import { Complex } from './../../models/complex.model';
import { baseUrl } from '../../config/api';
import { UserProvider } from './../user/user';

@Injectable()
export class StatsProvider {
  stats: StatsItem[];
  userId: string;

  constructor(
    public http: HTTP,
    private userProvider: UserProvider,
    private storage: Storage
  ) {
    this.userId = this.userProvider.userId;
    this.userProvider.userIdSubject$.subscribe(data => {
      this.userId = data;
    });
  }

  updateStats(complex: Complex) {
    let headers = {
      'Content-Type': 'application/json'
    };
    const result = new Observable(observer => {
      this.http.setDataSerializer('json');
      this.http
        .post(
          `${baseUrl}/users/${this.userId}/stats/`,
          {
            completed_complex: complex.id
          },
          headers
        )
        .then(data => {
          this.stats = JSON.parse(data.data);
          observer.next(this.stats);
        })
        .catch(error => {
          observer.error(error);
        });
    });
    return result;
  }

  getStatsFromServer() {
    const retrievedStats$ = Observable.from(
      this.http.get(`${baseUrl}/users/${this.userId}`, {}, {})
    );
    const result = new Observable<StatsItem[]>(observer => {
      retrievedStats$.subscribe(
        retrieved => {
          const retrievedData = JSON.parse(retrieved.data);
          const unique = [];

          retrievedData.complexes.forEach(element => {
            if (!this.isInComplex(unique, element)) {
              unique.push(element);
            }
          });

          this.stats = unique.map(item => {
            return new StatsItem(item);
          });

          this.saveStats(this.stats);
          observer.next(this.stats);
        },
        error => {
          observer.error(error);
        }
      );
    });
    return result;
  }

  getStatsFromLocal() {
    const localStats$ = Observable.from(this.storage.get('stats'));
    const result = new Observable<StatsItem[]>(observer => {
      localStats$.subscribe(
        data => {
          this.stats = data;
          this.saveStats(this.stats);
          observer.next(data);
        },
        error => {
          observer.error(error);
        }
      );
    });
    return result;
  }

  saveStats(stats) {
    this.storage.set('stats', stats);
  }

  private isInComplex(complexes, complex) {
    return complexes.find(item => {
      return item.id === complex.id;
    });
  }
}
