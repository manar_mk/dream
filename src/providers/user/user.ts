import { baseUrl } from './../../config/api';
import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';
import { HTTP } from '@ionic-native/http';
import { Subject } from 'rxjs';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  userId: string;
  userIdSubject$ = new Subject<string>();

  url = `${baseUrl}/users/`;
  constructor(public http: HTTP, private storage: Storage) {

  }

  retrieveUserId() {
    const result = new Observable<string>(observer => {
      this.storage
        .get('userId')
        .then(local => {
          if (local) {
            this.userId = local;
            observer.next(this.userId);
            this.userIdSubject$.next(this.userId);
          } else {
            this.http
              .post(this.url, {}, {})
              .then(result => {
                const user = JSON.parse(result.data);
                this.storage.set('userId', user.id).then(result => {
                  this.userId = user.id;
                  this.userIdSubject$.next(this.userId);
                  observer.next(this.userId);
                });
              })
              .catch(error => {
                console.error('userId get from API Error');
                console.error(error);
                observer.error(error);
              });
          }
        })
        .catch(error => {
          console.error('userId get Error');
          console.error(error);
          this.http
            .post(this.url, {}, {})
            .then(result => {
              const user = JSON.parse(result.data);
              this.storage
                .set('userId', user.id)
                .then(result => {
                  this.userId = user.id;
                  this.userIdSubject$.next(this.userId);
                  observer.next(this.userId);
                })
                .catch(error => {
                  console.error('userId set to storage Error');
                  console.error(error);
                  observer.error(error);
                });
            })
            .catch(error => {
              console.error('userId get from API Error');
              console.error(error);
              observer.error(error);
            });
        });
    });
    return result;
  }

  setDeviceToken(token, userId) {
    if (!token) return;
    let headers = {
      'Content-Type': 'application/json'
    };
    this.http.setDataSerializer('json');
    return this.http.post(
      `${this.url}${userId}/devices/`,
      { registration_id: token },
      headers
    );
  }
}
