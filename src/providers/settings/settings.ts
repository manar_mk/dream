import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SettingsProvider {
  currentMelody: any;
  currentVoice: any;

  soundsEnabled: boolean = true;
  soundsEnabledSubject$ = new Subject<boolean>();

  constructor(public http: HTTP, private storage: Storage) {
    this.storage
      .get('soundsEnabled')
      .then(value => {
        this.setSoundsEnabled(value);
      })
      .catch(error => {
        this.setSoundsEnabled(true);
      });
  }

  getVoice() {
    const result = new Observable<any>(observer => {
      this.storage
        .get('voice')
        .then(result => {
          this.currentVoice = result;
          if (result) {
            observer.next(this.currentVoice);
            observer.complete();
          } else {
            this.catchNoVoice(observer);
          }
        })
        .catch(() => {
          this.catchNoVoice(observer);
        });
    });
    return result;
  }

  setVoice(voice) {
    const result = new Observable<any>(observer => {
      this.storage
        .set('voice', voice)
        .then(result => {
          observer.next(result);
          observer.complete();
        })
        .catch(error => {
          observer.next(error);
          observer.complete();
        });
    });
    return result;
  }

  private catchNoVoice(observer) {
    this.currentVoice = 'male';
    this.setVoice('male').subscribe(data => {
      observer.next(this.currentVoice);
      observer.complete();
    });
  }

  setSoundsEnabled(value = true) {
    this.soundsEnabled = value;
    this.storage.set('soundsEnabled', this.soundsEnabled);
    this.soundsEnabledSubject$.next(value);
  }

  isSoundsEnabled() {
    return this.soundsEnabled;
  }
}
