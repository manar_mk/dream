import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { SettingsProvider } from '../settings/settings';

/*
  Generated class for the AudioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AudioProvider {
  exerciseStartAudio;
  exerciseEndAudio;
  exerciseNextAudio;

  currentVoice;

  isNotificationsEnabled = this.settingsProvider.isSoundsEnabled();

  public settingsMaleAudio = new Audio(`assets/audio/male/select.ogg`);
  public settingsFemaleAudio = new Audio(`assets/audio/female/select.ogg`);
  public settingsMelody1Audio = new Audio(`assets/audio/melody1/start.ogg`);
  public settingsMelody2Audio = new Audio(`assets/audio/melody2/start.ogg`);
  public settingsMelody3Audio = new Audio(`assets/audio/melody3/start.ogg`);

  constructor(
    private settingsProvider: SettingsProvider
  ) {
    this.settingsMaleAudio.load();
    this.settingsFemaleAudio.load();
    this.settingsMelody1Audio.load();
    this.settingsMelody2Audio.load();
    this.settingsMelody3Audio.load();

    this.settingsProvider.soundsEnabledSubject$.subscribe(data=>{
      this.isNotificationsEnabled = data;
    })
  }

  loadVoices() {
    const result = new Observable<any>(observer => {
      this.settingsProvider.getVoice().subscribe(voice => {
        this.exerciseStartAudio = new Audio(`assets/audio/${voice}/start.ogg`);
        this.exerciseStartAudio.load();

        this.exerciseEndAudio = new Audio(`assets/audio/${voice}/end.ogg`);
        this.exerciseEndAudio.load();

        if (this.isHumanVoice(voice)) {
          this.exerciseNextAudio = new Audio(`assets/audio/${voice}/next.ogg`);
          this.exerciseNextAudio.load();
        }

        this.currentVoice = voice;
        observer.next(voice);
        observer.complete();
      });
    });

    return result;
  }

  playExerciseStart() {
    console.log('playing exercise Start');
    // console.log(this.exerciseStartAudio);
    this.play(this.exerciseStartAudio);
  }

  playExerciseEnd() {
    console.log('playing exercise End');
    // console.log(this.exerciseEndAudio);
    this.play(this.exerciseEndAudio);
  }

  playExerciseNext() {
    if (this.isHumanVoice()) {
      this.play(this.exerciseNextAudio);
    }
  }

  stopExerciseStart() {
    console.log('stopping complex Start');

    this.stop(this.exerciseStartAudio);
  }

  stopExerciseEnd() {
    console.log('stopping complex End');

    this.stop(this.exerciseEndAudio);
  }

  stopExerciseNext() {
    console.log('stopping complex Next');

    this.stop(this.exerciseNextAudio);
  }

  playComplexStart() {
    // console.log('playing complex Start');
    // console.log(this.complexStartAudio);
    // this.play(this.complexStartAudio);
  }

  stopComplexStart() {
    // this.stop(this.complexStartAudio);
  }

  playComplexEnd() {
    // console.log('playing complex End');
    // console.log(this.complexEndAudio);
    // this.play(this.complexEndAudio);
  }

  stopComplexEnd() {
    // this.stop(this.complexEndAudio);
  }

  play(audio, force = false) {
    if (audio && (this.isNotificationsEnabled || force)) {
      audio.play();
    }
  }

  stop(audio) {
    if (audio) {
      audio.pause();
      audio.currentTime = 0;
    }
  }

  private isHumanVoice(voice = this.currentVoice) {
    return voice == 'male' || voice == 'female';
  }
}
