import { Platform } from 'ionic-angular';
import { Firebase } from '@ionic-native/firebase';
import { Injectable } from '@angular/core';
// import { AngularFirestore } from '@angular/fire/firestore';
import { UserProvider } from '../user/user';

/*
  Generated class for the FcmProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FcmProvider {
  constructor(
    public firebaseNative: Firebase,
    // public afs: AngularFirestore,
    private platform: Platform,
    private userProvider: UserProvider
  ) {}

  async getToken() {
    let token;

    if (this.platform.is('android')) {
      token = await this.firebaseNative.getToken();
    }
    if (this.platform.is('ios')) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }
    this.userProvider.retrieveUserId().subscribe(userId => {
      console.log(userId);
      this.userProvider
        .setDeviceToken(token, userId)
        .then(data => {
          console.log('deviceTokenResult');
          console.log(data);
          console.log(`token:${token} is now registering to userId:${userId}`)
          // return this.saveTokenToFirestore(token, userId);
          return data;
        })
        .catch(error => {
          console.error('deviceTokenResult');
          console.error(error);
          return error;
        });
    });
  }

  // private saveTokenToFirestore(token: string, userId) {
  //   if (!token) return;
  //   const devicesRef = this.afs.collection('devices');

  //   const docData = {
  //     token,
  //     userId: userId
  //   };

  //   return devicesRef.doc(token).set(docData);
  // }

  listenNotifications() {
    return this.firebaseNative.onNotificationOpen();
  }
}
