import { baseUrl } from './../../config/api';
import { UserProvider } from './../user/user';
import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import { map } from 'rxjs/operators';
import { Storage } from '@ionic/storage';

/*
  Generated class for the NotificationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationsProvider {
  url = `${baseUrl}/users/`;
  userId: string;

  constructor(
    public http: HTTP,
    private userProvider: UserProvider,
    private storage: Storage
  ) {
    this.userId = this.userProvider.userId;
    this.userProvider.userIdSubject$.subscribe(data => {
      this.userId = data;
    });
  }

  getNotificationsFromLocal() {
    const localNotifications$ = Observable.from(
      this.storage.get('notifications')
    );

    const result$ = new Observable(observer => {
      localNotifications$.subscribe(
        local => {
          observer.next(local);
        },
        error => {
          observer.error(error);
        }
      );
    });
    return result$;
  }

  getNotificationsFromServer() {
    const retrievedNotifications$ = Observable.from(
      this.http.get(this.constructUrl(this.userId), {}, {})
    ).pipe(
      map((result: any) => {
        return JSON.parse(result.data);
      })
    );

    const result$ = new Observable(observer => {
      retrievedNotifications$.subscribe(
        retrieved => {
          this.saveNotifications(retrieved);
          observer.next(retrieved);
        },
        error => {
          observer.error(error);
        }
      );
    });
    return result$;
  }

  createNotification(notification) {
    const userId$ = this.userProvider.retrieveUserId();
    const result$ = new Observable(observer => {
      userId$.subscribe(
        userId => {
          let headers = {
            'Content-Type': 'application/json'
          };
          this.http.setDataSerializer('json');
          this.http
            .post(this.constructUrl(userId), notification, headers)
            .then(data => {
              observer.next(JSON.parse(data.data));
            })
            .catch(error => {
              observer.error(error);
            });
        },
        error => {
          observer.error(error);
        }
      );
    });
    return result$;
  }

  deleteNotification(notification) {
    const userId$ = this.userProvider.retrieveUserId();
    const result$ = new Observable(observer => {
      userId$.subscribe(
        userId => {
          let headers = {
            'Content-Type': 'application/json'
          };
          this.http.setDataSerializer('json');
          this.http
            .delete(
              `${this.constructUrl(userId)}${notification.id}/`,
              {},
              headers
            )
            .then(data => {
              observer.next();
            })
            .catch(error => {
              observer.error(error);
            });
        },
        error => {
          observer.error(error);
        }
      );
    });
    return result$;
  }

  private constructUrl(userId) {
    return `${this.url}${userId}/settings/`;
  }

  updateNotification(notification) {
    const userId$ = this.userProvider.retrieveUserId();
    const result$ = new Observable(observer => {
      userId$.subscribe(
        userId => {
          let headers = {
            'Content-Type': 'application/json'
          };
          this.http.setDataSerializer('json');
          this.http
            .put(
              `${this.constructUrl(userId)}${notification.id}/`,
              notification,
              headers
            )
            .then(data => {
              observer.next(JSON.parse(data.data));
            })
            .catch(error => {
              observer.error(error);
            });
        },
        error => {
          observer.error(error);
        }
      );
    });
    return result$;
  }

  saveNotifications(notifications) {
    return this.storage.set('notifications', notifications);
  }
}
