import { HTTP } from '@ionic-native/http';
import { baseUrl } from './../../config/api';
import { Exercise } from './../../models/exercise.model';
import { Complex } from './../../models/complex.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/forkJoin';


import { map } from 'rxjs/operators';

import { Storage } from '@ionic/storage';

/*
  Generated class for the ComplexProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ComplexProvider {
  complexes: Complex[];
  url = `${baseUrl}/complexes/`;
  constructor(public http: HTTP, private storage: Storage) {}

  getComplexes() {
    const retrievedComplexes$ = Observable.from(
      this.http.get(this.url, {}, {})
    ).pipe(
      map((result: any) => {
        return JSON.parse(result.data);
      })
    );
    const localComplexes$ = Observable.from(this.storage.get('complexes'));
    const result = new Observable<any>(observer => {
      Observable.forkJoin(localComplexes$, retrievedComplexes$).subscribe(
        result => {
          const [local, retrieved] = result;
          if (retrieved) {
            this.complexes = retrieved.map(item => {
              return new Complex(item);
            });
          } else if(local){
            this.complexes = local.map(item => {
              return new Complex(item);
            });
          } else {
            this.complexes = [];
          }
          if(retrieved && local){
            this.complexes.map(complex=>{
              const localComplex = local.find(localItem=>{
                return localItem.id === complex.id;
              });
              if(localComplex && complex.exercises){
                complex.exercises.map(exercise=>{
                  const localExercise = localComplex.exercises.find(localExercise=>{
                    return localExercise.id === exercise.id;
                  });
                  if(localExercise){
                    exercise.duration = localExercise.duration;
                  }
                  return exercise;
                })
              }
              return complex;
            })
          }
          this.storage.set('complexes', this.complexes);
          observer.next(this.complexes);
        }
      );
    });
    return result;
  }

  updateTimer(exercise: Exercise, timerValue: number) {
    exercise.duration = timerValue;
    this.storage.set('complexes', this.complexes);
  }
}
