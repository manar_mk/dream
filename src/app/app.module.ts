import { NotificationsProvider } from './../providers/notifications/notifications';
import { Insomnia } from '@ionic-native/insomnia';
import { Firebase } from '@ionic-native/firebase';
import { FcmProvider } from './../providers/fcm/fcm';
import { AudioProvider } from './../providers/audio/audio';
import { SettingsProvider } from './../providers/settings/settings';
import { UserProvider } from './../providers/user/user';
import { StatsProvider } from './../providers/stats/stats';
import { SocialSharing } from '@ionic-native/social-sharing';
import { HTTP } from '@ionic-native/http';
import { ComplexProvider } from './../providers/complex/complex';
import { firebaseConfig } from './../config/api';
import { ComponentsModule } from './../components/components.module';
import { ComplexPageModule } from './../pages/complex/complex.module';
import { ComplexesPageModule } from './../pages/complexes/complexes.module';
import { StatsPageModule } from './../pages/stats/stats.module';
import { SettingsPageModule } from './../pages/settings/settings.module';
import { LearnPageModule } from './../pages/learn/learn.module';
import { ExercisePageModule } from './../pages/exercise/exercise.module';
import { CongratsPageModule } from './../pages/congrats/congrats.module';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { AngularFireModule } from '@angular/fire';
import { FCM } from '@ionic-native/fcm';

const PAGES = [
  CongratsPageModule,
  ExercisePageModule,
  LearnPageModule,
  SettingsPageModule,
  StatsPageModule,
  ComplexesPageModule,
  ComplexPageModule,
  LearnPageModule
]

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ComponentsModule,
    IonicStorageModule.forRoot(),
    PAGES,
    AngularFireModule.initializeApp(firebaseConfig),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ComplexProvider,
    HTTP,
    SocialSharing,
    StatsProvider,
    UserProvider,
    SettingsProvider,
    AudioProvider,
    FcmProvider,
    Firebase,
    Insomnia,
    NotificationsProvider,
    FCM
  ]
})
export class AppModule {}
