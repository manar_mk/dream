import { Component } from '@angular/core';
import { Platform, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { Firebase } from '@ionic-native/firebase';
import { Insomnia } from '@ionic-native/insomnia';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;
  regID: string;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private firebase: Firebase,
    public toastCtrl: ToastController,
    private insomnia: Insomnia
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.insomnia
        .allowSleepAgain()
        .then(
          () => console.log('allowed sleep again'),
          () => console.log('cant go to sleep')
        )
        .catch(error => console.error('cant go to sleep', error));
    });

    this.firebase
      .getToken()
      .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
      .catch(error => console.error('Error getting token', error));

    this.firebase
      .onTokenRefresh()
      .subscribe((token: string) => console.log(`Got a new token ${token}`));
  }
}
