import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { PipesModule } from './../pipes/pipes.module';
import { ComplexComponent } from './complex/complex';
import { ExerciseComponent } from './exercise/exercise';
import { LearnComponent } from './learn/learn';
import { CalendarComponent } from './calendar/calendar';
@NgModule({
  declarations: [
    ComplexComponent,
    ExerciseComponent,
    LearnComponent,
    CalendarComponent
  ],
  imports: [IonicModule, PipesModule],
  exports: [
    ComplexComponent,
    ExerciseComponent,
    LearnComponent,
    CalendarComponent
  ]
})
export class ComponentsModule {}
