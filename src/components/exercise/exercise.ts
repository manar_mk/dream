import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';

import { ExerciseModalPage } from './../../pages/exercise-modal/exercise-modal';
import { Exercise } from './../../models/exercise.model';

/**
 * Generated class for the ExerciseComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'exercise',
  templateUrl: 'exercise.html'
})
export class ExerciseComponent {
  @Input()
  exercise: Exercise;

  constructor(public modalCtrl: ModalController) {}

  getDurationView(duration: number) {
    if (duration) {
      const minutes = Math.floor(duration / 60);
      const seconds = duration % 60;
      return `${this.pad(minutes)}:${this.pad(seconds)}`;
    }
  }

  editDuration(duration: number) {
    const modal = this.modalCtrl.create(ExerciseModalPage, { exercise: this.exercise });
    modal.present();
  }

  private pad(num, size = 2) {
    var s = num + '';
    while (s.length < size) s = '0' + s;
    return s;
  }
}
