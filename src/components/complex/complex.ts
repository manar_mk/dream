import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NavController } from 'ionic-angular';

import { LearnPage } from './../../pages/learn/learn';
import { Complex } from './../../models/complex.model';
import { ComplexPage } from './../../pages/complex/complex';

/**
 * Generated class for the ComplexComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'complex',
  templateUrl: 'complex.html'
})
export class ComplexComponent {
  @Input()
  complex: Complex;

  constructor(public navCtrl: NavController, private sanitizer: DomSanitizer) {}

  openComplex(complex: Complex) {
    this.navCtrl.setRoot(ComplexPage, {
      complex: complex
    });
  }

  openLearn(complex: Complex) {
    this.navCtrl.setRoot(LearnPage, {
      complex: complex
    });
  }

  getVideoLink() {
    if (this.complex && this.complex.videoLink) {
      var video_id = this.complex.videoLink.split('v=')[1];
      var ampersandPosition = video_id.indexOf('&');
      if (ampersandPosition != -1) {
        video_id = video_id.substring(0, ampersandPosition);
      }

      return this.sanitizer.bypassSecurityTrustResourceUrl(
        `https://www.youtube.com/embed/${video_id}?enablejsapi=1&fs=1&playsinline=1`
      );
    }
    return '';
  }
}
