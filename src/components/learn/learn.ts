import { ComplexPage } from './../../pages/complex/complex';
import { Complex } from './../../models/complex.model';
import { NavController } from 'ionic-angular';
import { Exercise } from './../../models/exercise.model';
import { Component, Input } from '@angular/core';

/**
 * Generated class for the LearnComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'learn',
  templateUrl: 'learn.html'
})
export class LearnComponent {
  @Input()
  exercise: Exercise;

  @Input()
  complex: Complex;

  constructor(public navCtrl: NavController) {
  }

  openComplex() {
    this.navCtrl.setRoot(ComplexPage, {
      complex: this.complex
    });
  }
}
