import { Component, Input, OnChanges } from '@angular/core';

import * as moment from 'moment';
import 'moment/locale/ru';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the CalendarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'calendar',
  templateUrl: 'calendar.html'
})
export class CalendarComponent implements OnChanges {
  @Input()
  selectedDays: Date[] = [];
  loading: any;

  monthNames =
    'Январь, Февраль, Март, Апрель, Май, Июнь, Июль, Август, Сентябрь, Октябрь, Ноябрь, Декабрь';

  currentDate = moment().format('YYYY-MM');

  minDate = '2018-01-01';
  maxDate = this.currentDate;

  days = [];

  constructor(public loadingCtrl: LoadingController) {}

  ngOnChanges() {
    this.updateMonthDays();
  }

  currentDateChanged($event) {
    this.currentDate = $event;
    this.updateMonthDays();
  }

  updateMonthDays() {
    this.startLoading();
    this.days = this.appendPreviousDays();
    const daysLimit = moment(this.currentDate, 'YYYY-MM').daysInMonth();
    for (let index = 1; index <= daysLimit; index++) {
      const day = moment(this.currentDate, 'YYYY-MM')
        .date(index)
        .format('YYYY-MM-DD');
      let daysItem = {
        index: index,
        disabled: false,
        selected: this.isSelected(day)
      };
      this.days.push(daysItem);
    }
    const nextDays = this.appendNextDays();
    this.days = this.days.concat(nextDays);
    this.stopLoading();
  }

  private appendPreviousDays() {
    const prev = moment(this.currentDate, 'YYYY-MM').subtract(1, 'months');

    prev.date(prev.daysInMonth());

    const prevDays = [];

    while (prev.isoWeekday() < 7) {
      let daysItem = {
        index: prev.date(),
        disabled: true,
        selected: this.isSelected(prev.format('YYYY-MM-DD'))
      };
      prevDays.push(daysItem);
      prev.subtract(1, 'days');
    }

    return prevDays.reverse();
  }

  private appendNextDays() {
    const next = moment(this.currentDate, 'YYYY-MM')
      .add(1, 'months')
      .date(1);
    const nextDays = [];
    while (next.isoWeekday() > 1) {
      let daysItem = {
        index: next.date(),
        disabled: true,
        selected: this.isSelected(next.format('YYYY-MM-DD'))
      };
      nextDays.push(daysItem);
      next.add(1, 'days');
    }
    return nextDays;
  }

  private isSelected(day) {
    const found = this.selectedDays.find(item => {
      return (
        moment(item).format('YYYY-MM-DD') === moment(day).format('YYYY-MM-DD')
      );
    });
    if (found) {
      return true;
    }
    return false;
  }

  private startLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: 'Загрузка...'
      });
    }
    this.loading.present();
  }

  private stopLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
